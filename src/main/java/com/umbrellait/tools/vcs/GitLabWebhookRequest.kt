package com.umbrellait.tools.vcs

data class GitLabWebhookRequest(
    val object_kind: String?,
    val ref: String?,
    val project_id: String?,
    val project: ProjectDto?,
    val repository: RepositoryDto?,
    val commits: List<CommitDto>?
) {
    data class ProjectDto(
        val id: String?,
        val name: String?,
        val description: String?,
        val web_url: String?
    )

    data class RepositoryDto(
        val name: String?,
        val url: String?,
        val description: String?,
        val homepage: String?,
        val git_http_url: String?,
        val git_ssh_url: String?,
        val visibility_level: Int?
    )

    data class CommitDto(
        val id: String?,
        val title: String?,
        val message: String?
    )
}
