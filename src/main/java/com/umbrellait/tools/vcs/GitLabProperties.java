package com.umbrellait.tools.vcs;

import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "gitlab")
@ConstructorBinding
public class GitLabProperties {
    String user;
    String accessToken;

    public GitLabProperties(String user, String accessToken) {
        this.user = user;
        this.accessToken = accessToken;
    }

    CredentialsProvider getCredentialsProvider() {
        return new UsernamePasswordCredentialsProvider(user, accessToken);
    }
}
