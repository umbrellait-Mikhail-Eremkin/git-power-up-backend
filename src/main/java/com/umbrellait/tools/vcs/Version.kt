@file:Suppress("ReplaceGetOrSet")

package com.umbrellait.tools.vcs

data class Version(
    val major: Int,
    val minor: Int,
    val hotfix: Int
) {

    val isMajor
        get() = minor == 0 && hotfix == 0

    val isMinor
        get() = minor != 0 && hotfix == 0

    val isHotfix
        get() = hotfix != 0

    override fun toString(): String =
        when {
            isMajor -> "$major"
            isMinor -> "$major.$minor"
            isHotfix -> "$major.$minor.$hotfix"
            else -> ""
        }

    companion object {

        fun valueOf(verStr: String): Version {
            val parts = verStr.split(".")
            if (parts.isEmpty() || parts.size > 3) {
                throw IllegalArgumentException("Version should be in format XX.YY.ZZ")
            }
            return Version(
                parts.get(0).toInt(),
                parts.takeIf { it.size > 1 }?.get(1)?.toInt() ?: 0,
                parts.takeIf { it.size > 2 }?.get(2)?.toInt() ?: 0
            )
        }
    }
}
