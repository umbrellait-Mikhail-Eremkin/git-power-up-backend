package com.umbrellait.tools.vcs

import com.github.lianjiatech.retrofit.spring.boot.exception.RetrofitException
import com.umbrellait.tools.vcs.gitlab.GitLabHttpApi
import com.umbrellait.tools.vcs.gitlab.dto.CreateMergeRequestParams
import org.eclipse.jgit.api.*
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.transport.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.io.File


@RestController
@RequestMapping("/gitlab")
class GitLabController {

    companion object {
        const val REMOTE_NAME = "origin"
        private val logger = LoggerFactory.getLogger(GitLabController::class.java)
    }

    @Autowired
    private lateinit var gitLabHttpApi: GitLabHttpApi

    @Autowired
    private lateinit var gitLabProperties: GitLabProperties

    @PostMapping("/trigger-push")
    fun processPush(@RequestBody request: GitLabWebhookRequest): BaseResponse {
        if (request.object_kind != "push") {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Unsupported event type: ${request.object_kind}")
        }

        if (request.ref == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ref is null")
        }
        val ref = request.ref

        if(request.project == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "project is null")
        }
        val project = request.project

        if (!ref.matches(Regex(".*release\\/[\\d\\.]+"))) {
            logger.info("Project ${project.id}. Unsupported branch: $ref, skipped")
            return BaseResponse("Unsupported branch: $ref, skipped")
        }

        if (request.commits?.all { it.message?.contains("Increase version") == true } == true) {
            logger.info("Project ${project.id}. Increment version commit, skipped")
            return BaseResponse("Increment version commit, skipped")
        }

        val git = getGit(request)

        val sourceBranch = "release/${ref.substringAfterLast("/")}"
        val version = Version.valueOf(sourceBranch.substringAfterLast("/"))

        git.fetch()
            .setRemote(REMOTE_NAME)
            .setCredentialsProvider(gitLabProperties.credentialsProvider)
            .call()

        if(git.repository.resolve("$REMOTE_NAME/$sourceBranch") == null) {
            logger.error("Project ${project.id}. Source branch '$sourceBranch' is not found in remote repository")
            git.close()
            throw ResponseStatusException(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "Source branch '$sourceBranch' is not found in remote repository"
            )
        }

        val targetBranch = when {
            version.isMajor && git.repository.resolve("$REMOTE_NAME/release/${version.major + 1}") == null -> "main"
            version.isMajor -> "release/${version.major + 1}"
            else -> ""
        }
        if (targetBranch.isEmpty()) {
            logger.warn("Project ${project.id}. Target branch is not found for source branch: $sourceBranch")
            git.close()
            throw ResponseStatusException(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "Target branch is not found for source branch: $sourceBranch"
            )
        }

        if (git.repository.resolve(targetBranch) == null) {
            git.checkout()
                .setCreateBranch(true)
                .setName(targetBranch)
                .setStartPoint("$REMOTE_NAME/$targetBranch")
                .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                .call()
        } else {
            git.checkout()
                .setName(targetBranch)
                .call()
            git.pull()
                .setRemote(REMOTE_NAME)
                .setCredentialsProvider(gitLabProperties.credentialsProvider)
                .call()
        }

        val mergeResult = git.merge()
            .include(git.repository.resolve("$REMOTE_NAME/$sourceBranch"))
            .setCommit(true)
            .setFastForward(MergeCommand.FastForwardMode.NO_FF)
            .setMessage("Automerge $sourceBranch -> $targetBranch")
            .call()

        val result = if (mergeResult.mergeStatus.isSuccessful) {
            git.push()
                .setRemote(REMOTE_NAME)
                .setCredentialsProvider(gitLabProperties.credentialsProvider)
                .call()
            logger.info("Project ${project.id}. Branch $targetBranch was updated from $sourceBranch")
            BaseResponse("Branch $targetBranch was updated from $sourceBranch")
        } else {
            git.reset()
                .setMode(ResetCommand.ResetType.HARD)
                .call()
            try {
                gitLabHttpApi.createMergeRequest(
                    request.project_id,
                    gitLabProperties.accessToken,
                    CreateMergeRequestParams(
                        id = request.project_id!!,
                        source_branch = sourceBranch,
                        target_branch = targetBranch,
                        title = "Automerge conflict $sourceBranch -> $targetBranch"
                    )
                )
            } catch (ex: RetrofitException) {
                logger.warn("Project ${project.id}. Retrofit exception: ${ex.message}")
            }
            logger.warn("Project ${project.id}. Automerge conflict $sourceBranch -> $targetBranch")
            BaseResponse("Automerge conflict $sourceBranch -> $targetBranch")
        }
        git.close()
        return result
    }

    private fun getGit(request: GitLabWebhookRequest): Git =
        request.project_id?.let { project_id ->
            val projectDir = File("trigger-push/$project_id")
            if (!projectDir.exists()) {
                logger.info("Created directory for project: $project_id, path: ${projectDir.path}")
                projectDir.mkdirs()
            }
            File(projectDir, ".git").let { dir ->
                if (!dir.exists()) {
                    dir.mkdir()
                    val repository = FileRepositoryBuilder.create(dir)
                        .apply { create() }
                    Git(repository).apply {
                        request.repository?.git_http_url?.let { http_url ->
                            logger.info("Project $project_id. Remote: $http_url")
                            remoteAdd()
                                .setName(REMOTE_NAME)
                                .setUri(URIish(http_url))
                                .call()
                        }
                    }
                } else {
                    val repository = FileRepositoryBuilder()
                        .setGitDir(dir)
                        .build()
                    Git(repository)
                }
            }
        } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "project_id is null")
}