package com.umbrellait.tools.vcs.email;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
