package com.umbrellait.tools.vcs.gitlab.dto

class CreateMergeRequestResponse{
    var id: String? = null
    var iid: String? = null
    var project_id: String? = null
}
