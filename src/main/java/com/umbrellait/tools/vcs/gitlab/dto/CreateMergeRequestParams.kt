package com.umbrellait.tools.vcs.gitlab.dto

data class CreateMergeRequestParams(
    val id: String,
    val source_branch: String,
    val target_branch: String,
    val title: String
)
