package com.umbrellait.tools.vcs.gitlab;

import com.github.lianjiatech.retrofit.spring.boot.core.RetrofitClient;
import com.umbrellait.tools.vcs.gitlab.dto.CreateMergeRequestParams;
import com.umbrellait.tools.vcs.gitlab.dto.CreateMergeRequestResponse;
import retrofit2.Response;
import retrofit2.http.*;

//@RetrofitClient(baseUrl = "${gitlab.baseUrl}")
@RetrofitClient(baseUrl = "https://gitlab.com/api/v4/")
public interface GitLabHttpApi {

    @POST("projects/{project_id}/merge_requests")
    Response<CreateMergeRequestResponse> createMergeRequest(
            @Path(value = "project_id", encoded = true) String project_id,
            @Query(value = "access_token") String access_token,
            @Body CreateMergeRequestParams params);
}
